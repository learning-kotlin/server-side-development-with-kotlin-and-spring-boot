package org.elu.learning.kotlin

data class KPet(var name: String, var age: Int, var ownerName: String)
