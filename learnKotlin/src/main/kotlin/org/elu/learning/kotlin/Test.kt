package org.elu.learning.kotlin

fun main(args: Array<String>) {
    // val cannot be changed
    val x = 10
    println(x)

    // var is mutable
    var y = 10
    y = 15
    println(y)

    var name: String = "Edu"
    // following line will cause compilation error, because null cannot be assigned to non-null type
    // name = null
    println(name)

    // to assign null, nullable type should be used, it's denoted with question mark
    var title: String? = "Mister"
    title = null
    // to access nullable variable
    println(title?.length)

    val firstName = "Edu"
    val lastName = "Finn"
    name = "$firstName $lastName"
    println(name)
}
