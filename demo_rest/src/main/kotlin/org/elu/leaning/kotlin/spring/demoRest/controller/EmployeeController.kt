package org.elu.leaning.kotlin.spring.demoRest.controller

import org.elu.leaning.kotlin.spring.demoRest.model.EmployeeDTO
import org.elu.leaning.kotlin.spring.demoRest.model.EmployeeUpdateReq
import org.elu.leaning.kotlin.spring.demoRest.service.DepartmentService
import org.elu.leaning.kotlin.spring.demoRest.service.EmployeeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.toFlux
import javax.validation.Valid

@RestController
class EmployeeController {

    @Autowired
    lateinit var employeeService: EmployeeService

    @Autowired
    lateinit var departmentService: DepartmentService

    @GetMapping("/employee/{id}")
    fun getEmployee(@PathVariable("id") id: String) = employeeService.getEmployee(id)

    @GetMapping("/employee")
    fun getEmployees(@RequestParam("minAge", required = false) minAge: Int?,
                     @RequestParam("minSalary", required = false) minSalary: Double?) =
            employeeService.getAllEmployees(minAge, minSalary)

    @PostMapping("/employee")
    fun createEmployee(@Valid @RequestBody employee: EmployeeDTO) =
            employeeService.createEmployee(employee)
                    .map { newEmployee -> ResponseEntity.status(HttpStatus.CREATED).body(newEmployee) }

    @GetMapping("/departments")
    fun getAllDepartments() = departmentService.getAllDepartments().toFlux()

    @PutMapping("/employee/{id}")
    fun updateEmployee(@Valid @PathVariable("id") id: String,
                       @RequestBody updateEmployee: EmployeeUpdateReq) =
            employeeService.updateEmployee(id, updateEmployee)
                    .map { _ -> ResponseEntity.ok() }

    @DeleteMapping("/employee/{id}")
    fun deleteEmployee(@PathVariable id: String) =
            employeeService.deleteEmployee(id)
                    .map { _ -> ResponseEntity.noContent() }
}
