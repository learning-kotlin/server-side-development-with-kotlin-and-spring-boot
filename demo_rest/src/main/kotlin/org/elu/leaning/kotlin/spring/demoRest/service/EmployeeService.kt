package org.elu.leaning.kotlin.spring.demoRest.service

import org.elu.leaning.kotlin.spring.demoRest.model.EmployeeDTO
import org.elu.leaning.kotlin.spring.demoRest.model.EmployeeUpdateReq
import org.elu.leaning.kotlin.spring.demoRest.repository.EmployeeRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class EmployeeService {
    @Autowired
    lateinit var employeeDb: EmployeeRepo

    fun createEmployee(employeeDTO: EmployeeDTO) = employeeDb.save(EmployeeDTO.newEmployee(employeeDTO))

    fun getEmployee(id: String) = employeeDb.findById(id)

    fun getAllEmployees(minAge: Int? = 0, minSalary: Double? = 0.0) =
            employeeDb.findAll()
                    .filter { it.age >= minAge ?: 0 }
                    .filter { it.salary >= minSalary ?: 0.0 }

    fun updateEmployee(id: String, updateReq: EmployeeUpdateReq) =
            employeeDb.findById(id).flatMap {
                it.department = updateReq.department ?: it.department
                it.salary = updateReq.salary ?: it.salary
                employeeDb.save(it)
            }

    fun deleteEmployee(id: String) = employeeDb.deleteById(id)
}
