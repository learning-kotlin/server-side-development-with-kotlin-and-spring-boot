package org.elu.leaning.kotlin.spring.demoRest.repository

import org.elu.leaning.kotlin.spring.demoRest.model.Employee
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository
import org.springframework.stereotype.Repository

@Repository
interface EmployeeRepo: ReactiveCassandraRepository<Employee, String>
